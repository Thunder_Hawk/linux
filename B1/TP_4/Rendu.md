# TP4 : Real services

## Partie 1 : Partitionnement du serveur de stockage

```bash
[user@storage ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda           8:0    0    8G  0 disk 
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part 
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    2G  0 disk 
sr0          11:0    1 1024M  0 rom  
[user@storage ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.

[user@storage ~]$ sudo vgcreate storage /dev/sdb
  Volume group "storage" successfully created

[user@storage ~]$ sudo vgs
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VBecfa08ec-7767e606_ PVID JYYcnmlaYUZIc2EhyGruk5ia66EYz2QY last seen on /dev/sda2 not found.
  VG      #PV #LV #SN Attr   VSize  VFree 
  storage   1   0   0 wz--n- <2.00g <2.00g
[user@storage ~]$ sudo vgdisplay
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VBecfa08ec-7767e606_ PVID JYYcnmlaYUZIc2EhyGruk5ia66EYz2QY last seen on /dev/sda2 not found.
  --- Volume group ---
  VG Name               storage
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <2.00 GiB
  PE Size               4.00 MiB
  Total PE              511
  Alloc PE / Size       0 / 0   
  Free  PE / Size       511 / <2.00 GiB
  VG UUID               3H0tS4-wND8-6o2o-FiIo-hD6y-O9dv-maYJFP

[user@storage ~]$ sudo lvcreate -l 100%FREE storage -n C
  Logical volume "C" created.
[user@storage ~]$ sudo mkfs -t ext4 /dev/storage/C
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 523264 4k blocks and 130816 inodes
Filesystem UUID: d84c2fee-9350-4f58-a0e9-c6c142c8270e
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done 

[user@storage ~]$ sudo mkdir /mnt/storage
[user@storage ~]$ sudo mount /dev/storage/C /mnt/storage
[user@storage ~]$ df -h | grep C
/dev/mapper/storage-C  2.0G   24K  1.9G   1% /mnt/storage

[user@storage ~]$ sudo vim /mnt/storage/text
[user@storage ~]$ cat /mnt/storage/text
Alea jacta est
[user@storage ~]$ sudo rm /mnt/storage/text

[user@storage ~]$ sudo vim /etc/fstab
[user@storage ~]$ cat /etc/fstab | tail -n 1
/dev/storage/c /mnt/storage ext4 defaults 0 0
[user@storage ~]$ sudo umount /mnt/storage
[user@storage ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/storage does not contain SELinux labels.
       You just mounted a file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/storage             : successfully mounted

```

## Partie 2 : Serveur de partage de fichiers

```bash
[user@storage ~]$ sudo dnf install nfs-utils
```

```bash
[user@web ~]$ sudo dnf install nfs-utils
```

```bash
[user@storage ~]$ sudo mkdir /mnt/storage/site_web_1
[user@storage ~]$ sudo mkdir /mnt/storage/site_web_2
[user@storage ~]$ sudo chown nobody /mnt/storage/site_web_2
[user@storage ~]$ sudo chown nobody /mnt/storage/site_web_1

```

```bash
[user@storage ~]$ sudo vim /etc/exports
[user@storage ~]$ sudo cat /etc/exports
#directory_to_share    client(share_option1,...,share_optionN)

/mnt/storage/site_web_1 10.3.1.9(rw,sync)
/mnt/storage/site_web_2 10.3.1.9(rw,sync)

[user@storage ~]$ sudo systemctl enable nfs-server
[user@storage ~]$ sudo systemctl start nfs-server
[user@storage ~]$ sudo systemctl status  nfs-server | grep active
     Active: active (exited) since Tue 2023-01-10 17:35:26 CET; 5min ago
    Process: 11491 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy ; fi (code=exited, status=0/SUCCESS)

[user@storage ~]$ sudo firewall-cmd --permanent --list-all | grep services
  services: cockpit dhcpv6-client ssh
[user@storage ~]$ sudo firewall-cmd --permanent --add-service=nfs && sudo firewall-cmd --permanent --add-service=mountd && sudo firewall-cmd --permanent --add-service=rpc-bind && sudo firewall-cmd --reload
success
success
success
success

[user@storage ~]$ sudo firewall-cmd --permanent --list-all | grep services
  services: cockpit dhcpv6-client mountd nfs rpc-bind ssh

```

```bash
[user@web ~]$ sudo mount 10.3.1.8:/mnt/storage/site_web_1 /var/www/site_web_1
[user@web ~]$ sudo mount 10.3.1.8:/mnt/storage/site_web_2 /var/www/site_web_2
[user@web ~]$ df -h | grep 10.3.1.8
10.3.1.8:/mnt/storage/site_web_1  2.0G     0  1.9G   0% /var/www/site_web_1
10.3.1.8:/mnt/storage/site_web_2  2.0G     0  1.9G   0% /var/www/site_web_2
[user@web ~]$ sudo touch /var/www/site_web_1/test
[user@web ~]$ ls -l  /var/www/site_web_1/
total 0
-rw-r--r--. 1 nobody nobody 0 Jan 10 18:17 test

[user@web ~]$ sudo vim /etc/fstab
[user@web ~]$ cat  /etc/fstab | tail -n 2
10.3.1.8:/mnt/storage/site_web_1 /var/www/site_web_1 nfs defaults 0 0
10.3.1.8:/mnt/storage/site_web_2 /var/www/site_web_2 nfs defaults 0 0

```

## Partie 3 : Serveur web

```bash
[user@web ~]$ sudo dnf install nginx
[user@web ~]$ sudo systemctl start nginx
[user@web ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[user@web ~]$ sudo systemctl status  nginx | grep active
     Active: active (running) since Mon 2023-01-16 14:18:02 CET; 34s ago

[user@web ~]$ ps -ef | grep nginx
root        1124       1  0 14:18 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1125    1124  0 14:18 ?        00:00:00 nginx: worker process
user        1233     916  0 14:30 pts/0    00:00:00 grep --color=auto nginx

[user@web ~]$ sudo ss -alnp4 | grep nginx
tcp   LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1125,fd=6),("nginx",pid=1124,fd=6))

[user@web ~]$ cat /etc/nginx/nginx.conf | grep share/nginx
include /usr/share/nginx/modules/*.conf;
        root         /usr/share/nginx/html;
#        root         /usr/share/nginx/html;

[user@web html]$ ls -l
total 12
-rw-r--r--. 1 root root 3332 Oct 31 16:35 404.html
-rw-r--r--. 1 root root 3404 Oct 31 16:35 50x.html
drwxr-xr-x. 2 root root   27 Jan 16 14:16 icons
lrwxrwxrwx. 1 root root   25 Oct 31 16:37 index.html -> ../../testpage/index.html
-rw-r--r--. 1 root root  368 Oct 31 16:35 nginx-logo.png
lrwxrwxrwx. 1 root root   14 Oct 31 16:37 poweredby.png -> nginx-logo.png
lrwxrwxrwx. 1 root root   37 Oct 31 16:37 system_noindex_logo.png -> ../../pixmaps/system-noindex-logo.png
[user@web ~]$ cd /usr/share/nginx/html/

```

```bash
[user@web html]$ cd
[user@web ~]$ sudo firewall-cmd --list-all | grep "ports"
  ports: 
  forward-ports: 
  source-ports: 
[user@web ~]$ sudo firewall-cmd --add-port 80/tcp --permanent
success
[user@web ~]$ sudo firewall-cmd --reload
success
[user@web ~]$ sudo firewall-cmd --list-all | grep "ports"
  ports: 80/tcp
  forward-ports: 
  source-ports:


```

```bash
 toto@fedora37  ~  curl 10.3.1.9 | grep "Rocky Linux"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620        <title>HTTP Server Test Page powered by: Rocky Linux</title>
0     0  11.9M      0 --:--:-- --:--:-- --:--:-- 7441k
            an HTTP server after it has been installed on a Rocky Linux system.
          <p>The Rocky Linux distribution is a stable and reproduceable platform
          <li>Neither the <strong>Rocky Linux Project</strong> nor the
          <li>The Rocky Linux Project nor the <strong>RESF</strong> have
        <p>For more information about Rocky Linux, please visit the
          <a href="https://rockylinux.org/"><strong>Rocky Linux
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src="icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->

```

```bash
[user@web log]$ sudo ls -l /var/log/nginx
total 16
-rw-r--r--. 1 root root 10711 Jan 16 15:00 access.log
-rw-r--r--. 1 root root   239 Jan 16 14:56 error.log
[user@web log]$ sudo cat /var/log/nginx/access.log | tail -n 3
10.3.1.1 - - [16/Jan/2023:15:00:45 +0100] "GET / HTTP/1.1" 200 7620 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0" "-"
10.3.1.1 - - [16/Jan/2023:15:00:45 +0100] "GET /icons/poweredby.png HTTP/1.1" 200 15443 "http://10.3.1.9/" "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0" "-"
10.3.1.1 - - [16/Jan/2023:15:00:45 +0100] "GET /poweredby.png HTTP/1.1" 200 368 "http://10.3.1.9/" "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0" "-"

```

```bash
[user@web ~]$ sudo firewall-cmd --remove-port 80/tcp --permanent
success
[user@web ~]$ sudo firewall-cmd --add-port 8080/tcp --permanent
success
[user@web ~]$ sudo firewall-cmd --reload
success
[user@web ~]$ sudo firewall-cmd --list-all | grep ports
  ports: 8080/tcp
  forward-ports: 
  source-ports: 

[user@web ~]$ sudo vim  /etc/nginx/nginx.conf
[user@web ~]$ cat  /etc/nginx/nginx.conf | grep 8080
        listen       8080;
        listen       [::]:8080;

[user@web ~]$ sudo systemctl restart nginx
[user@web ~]$ systemctl status nginx | grep active
     Active: active (running) since Mon 2023-01-16 15:14:51 CET; 2min 13s ago
[user@web ~]$ sudo ss -alpn4 | grep nginx
tcp   LISTEN 0      511          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=1479,fd=6),("nginx",pid=1477,fd=6))

```

```bash 
 toto@fedora37  ~  curl 10.3.1.9:8080 | grep "Test page"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0  9552k      0 --:--:-- --:--:-- --:--:-- 7441k

```

```bash
[user@web ~]$ sudo useradd web -p web

[user@web ~]$ sudo vim  /etc/nginx/nginx.conf
[user@web nginx]$ sudo cat  /etc/nginx/nginx.conf | grep web
user web;
[user@web nginx]$ sudo systemctl restart nginx

[user@web nginx]$ ps -ef | grep web
web         1555    1554  0 15:52 ?        00:00:00 nginx: worker process
user        1559     916  0 15:53 pts/0    00:00:00 grep --color=auto web
```

```bash
[user@web ~]$ sudo vim /var/www/site_web_1/index.html
[user@web ~]$ sudo vim  /etc/nginx/nginx.conf
[user@web ~]$ cat  /etc/nginx/nginx.conf | grep root
        root         /var/www/site_web_1/;
#        root         /usr/share/nginx/html;
[user@web ~]$ sudo systemctl reload nginx

```

```bash
 toto@fedora37  ~  curl 10.3.1.9:8080 | grep 404
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   532  100   532    0     0   360k      0 --:--:-- --:--:-- --:--:--  519k
    <div class="container white black-text p404"></div>
      404

```

```bash
[user@web ~]$ cat  /etc/nginx/nginx.conf | grep conf.d
    # Load modular configuration files from the /etc/nginx/conf.d directory.
    include /etc/nginx/conf.d/*.conf;

[user@web ~]$ sudo vim  /etc/nginx/conf.d/site_web_1.conf
[user@web ~]$ cat  /etc/nginx/conf.d/site_web_1.conf | grep 8080
        listen       8080;
        listen       [::]:8080;


```

```bash
[user@web ~]$ sudo vim /var/www/site_web_2/index.html
[user@web ~]$ cat /var/www/site_web_2/index.html | grep test
    <title>Ma page test</title>
[user@web ~]$ sudo vim  /etc/nginx/conf.d/site_web_2.conf
[user@web ~]$ cat  /etc/nginx/conf.d/site_web_2.conf | head -n 5
    server {
        listen       8888;
        listen       [::]:8888;
        server_name  _;
        root         /var/www/site_web_2/;

[user@web ~]$ sudo systemctl reload nginx

[user@web ~]$ sudo firewall-cmd --add-port 8888/tcp --permanent
success
[user@web ~]$ sudo firewall-cmd --reload
success

```

```bash
 ✘ toto@fedora37  ~  curl 10.3.1.9:8080 | grep 404
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   532  100   532    0     0   190k      0 --:--:-- --:--:-- --:--:--  259k
    <div class="container white black-text p404"></div>
      404


 toto@fedora37  ~  curl 10.3.1.9:8888 | grep test
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   154  100   154    0     0   182k      0 --:--:-- --:--:-- --:--:--  150k
    <title>Ma page test</title>

```

