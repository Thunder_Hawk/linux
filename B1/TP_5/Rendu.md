# TP5 : Self-hosted cloud

## Partie 1 : Mise en place et maîtrise du serveur Web

### 1. Installation

```bash
[user@web ~]$ sudo dnf install httpd
[user@web ~]$ sudo vim /etc/httpd/conf/httpd.conf

[user@web ~]$ sudo systemctl start httpd
[user@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.


[user@web ~]$ sudo ss -alnpt | grep httpd
LISTEN 0      511                *:80              *:*    users:(("httpd",pid=1348,fd=4),("httpd",pid=1347,fd=4),("httpd",pid=1346,fd=4),("httpd",pid=1340,fd=4))

[user@web ~]$ sudo firewall-cmd --add-port 80/tcp --permanent
success
[user@web ~]$ sudo firewall-cmd --reload
success
[user@web ~]$ sudo firewall-cmd --list-all | grep ports
  ports: 80/tcp
```

```bash
[user@web /]$ sudo systemctl is-enabled httpd
enabled

[user@web ~]$ sudo systemctl status httpd | grep active
     Active: active (running) since Tue 2023-01-17 10:21:24 CET; 7min ago

[user@web ~]$ curl localhost | tail -n 5
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0  1063k      0 --:--:-- --:--:-- --:--:-- 1063k
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>
      
  </body>
</html>

```

```bash
 toto@fedora37  ~  curl 10.105.1.11 | tail -n 5
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0  9614k      0 --:--:-- --:--:-- --:--:-- 7441k
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>
      
  </body>
</html>

```
### 2. Avancer vers la maîtrise du service

```bash
[user@web ~]$ cd /
[user@web /]$ cat  /usr/lib/systemd/system/httpd.service | tail -n 20
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target

```

```bash
[user@web /]$ cat /etc/httpd/conf/httpd.conf | grep User | head -n 1
User apache

[user@web /]$ ps -ef | grep apache
apache      1345    1340  0 10:21 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1346    1340  0 10:21 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1347    1340  0 10:21 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1348    1340  0 10:21 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
user        1713     968  0 10:45 pts/0    00:00:00 grep --color=auto apache

[user@web /]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Jan 17 10:16 .
drwxr-xr-x. 82 root root 4096 Jan 17 10:16 ..
-rw-r--r--.  1 root root 7620 Jul 27 20:05 index.html

```

```bash
[user@web /]$ cat /etc/passwd | grep apache
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin

[user@web /]$ sudo useradd tiger -m -d /usr/share/httpd -s /usr/sbin/nologin
useradd: warning: the home directory /usr/share/httpd already exists.
useradd: Not copying any file from skel directory into it.
Creating mailbox file: File exists

[user@web /]$ sudo vim /etc/httpd/conf/httpd.conf
[user@web /]$ cat /etc/httpd/conf/httpd.conf | grep User | head -n 1
User tiger

[user@web /]$ sudo systemctl reload httpd
[user@web /]$ ps -ef | grep httpd
root        1340       1  0 10:20 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
tiger       1768    1340  0 11:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
tiger       1769    1340  0 11:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
tiger       1770    1340  0 11:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
tiger       1771    1340  0 11:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
user        1984     968  0 11:00 pts/0    00:00:00 grep --color=auto httpd

```

```bash
[user@web /]$ sudo vim /etc/httpd/conf/httpd.conf
[user@web /]$ cat /etc/httpd/conf/httpd.conf | grep Listen
Listen 8888

[user@web /]$ sudo firewall-cmd --remove-port 80/tcp  --permanent
success
[user@web /]$ sudo firewall-cmd --add-port 8888/tcp  --permanent
success
[user@web /]$ sudo firewall-cmd --reload
success

[user@web /]$ sudo systemctl reload httpd
[user@web /]$ sudo ss -altnp4
State         Recv-Q        Send-Q                 Local Address:Port                 Peer Address:Port        Process        
LISTEN        0             128                          0.0.0.0:22                        0.0.0.0:*            users:(("sshd",pid=686,fd=3))
[user@web /]$ sudo ss -alnpt | grep httpd
LISTEN 0      511                *:8888            *:*    users:(("httpd",pid=2041,fd=6),("httpd",pid=2040,fd=6),("httpd",pid=2039,fd=6),("httpd",pid=1340,fd=6))
[user@web /]$ curl localhost:8888 | tail -n 3
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0   826k      0 --:--:-- --:--:-- --:--:--  826k
      
  </body>
</html>

```

```bash
 toto@fedora37  ~  curl 10.105.1.11:8888 | tail -n 3
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0  5599k      0 --:--:-- --:--:-- --:--:-- 7441k
      
  </body>
</html>

```
[Fichier de conf](/etc/httpd/conf/httpd.conf)

## Partie 2 : Mise en place et maîtrise du serveur de base de données

```bash
[user@db ~]$ sudo dnf install mariadb-server -y
[user@db ~]$ sudo systemctl enable mariadb && sudo systemctl start mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

[user@db ~]$ sudo mysql_secure_installation
[user@db ~]$ sudo systemctl is-enabled mariadb
enabled

```

```bash
[user@db ~]$ sudo ss -alnp | grep maria
u_str LISTEN 0      80                      /var/lib/mysql/mysql.sock 26088                  * 0    users:(("mariadbd",pid=3153,fd=20))                                                          
tcp   LISTEN 0      80                                              *:3306                   *:*    users:(("mariadbd",pid=3153,fd=19))                 

[user@db ~]$ sudo firewall-cmd --add-port 3306/tcp --permanent
success
[user@db ~]$ sudo firewall-cmd --reload
success

```

```bash
[user@db ~]$ ps -ef | grep maria
mysql       3153       1  0 11:17 ?        00:00:00 /usr/libexec/mariadbd --basedir=/usr
user        3265     860  0 11:25 pts/0    00:00:00 grep --color=auto maria

```

## Partie 3 : Configuration et mise en place de NextCloud

### 1. Base de données

```bash
[user@db ~]$ sudo mysql -u root -p

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.105.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.105.1.11';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

```

```bash
[user@db ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] y
Enabled successfully!
Reloading privilege tables..
 ... Success!


You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] y
New password: 
Re-enter new password: 
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] 
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] 
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] 
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!

```

```bash
[user@web /]$ sudo dnf install mysql -y
[user@web /]$ mysql -u nextcloud -h 10.105.1.12 -p

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;
Database changed
mysql> SHOW TABLES;
Empty set (0.00 sec)
mysql> exit
Bye
```

```bash
[user@db ~]$ sudo mysql -u root -p

MariaDB [(none)]> use mysql;

MariaDB [mysql]> select User,Password,host from user;
+-------------+-------------------------------------------+-------------+
| User        | Password                                  | Host        |
+-------------+-------------------------------------------+-------------+
| mariadb.sys |                                           | localhost   |
| root        | *D5D9F81F5542DE067FFF5FF7A4CA4BDD322C578F | localhost   |
| mysql       | invalid                                   | localhost   |
| nextcloud   | *AF136CF35F0D546F69717A7F18C18849666E64D0 | 10.105.1.11 |
+-------------+-------------------------------------------+-------------+
4 rows in set (0.001 sec)
mysql> exit
Bye

```
### 2. Serveur Web et NextCloud

```bash
[user@web /]$ sudo vim /etc/httpd/conf/httpd.conf
[user@web /]$ cat /etc/httpd/conf/httpd.conf | grep User | head -n 1 && cat /etc/httpd/conf/httpd.conf | grep Listen
User apache
Listen 80

[user@web /]$ sudo firewall-cmd --remove-port 8888/tcp --permanent && sudo firewall-cmd --add-port 80/tcp --permanent
success
success
[user@web /]$ sudo firewall-cmd --reload
success

[user@web /]$ sudo systemctl reload httpd && sudo systemctl status httpd | grep active
     Active: active (running) since Tue 2023-01-17 10:21:24 CET; 1h 42min ago

```

```bash
[user@web /]$ sudo dnf config-manager --set-enabled crb
[user@web /]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
[user@web /]$ dnf module list php -y
[user@web /]$ sudo dnf module enable php:remi-8.1 -y
[user@web /]$ sudo dnf install -y php81-php -y
[user@web /]$  sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp

```

```bash
[user@web /]$ sudo mkdir /var/www/tp5_nextcloud

[user@web /]$ sudo curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip --output /tmp/TP_5

[user@web /]$ sudo dnf install unzip -y

[user@web /]$ sudo unzip /tmp/TP_5 -d /var/www/
[user@web /]$ sudo mv /var/www/nextcloud/ /var/www/tp5_nextcloud/
[user@web /]$ sudo rm -r /tmp/TP_5 

```

```bash
[user@web /]$ cat /var/www/tp5_nextcloud/index.html  | tail -n 4
	<script> window.location.href="index.php"; </script>
	<meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>

[user@web /]$ sudo chown  apache /var/www/tp5_nextcloud/
[user@web ~]$ sudo chown -R apache:apache /var/www/tp5_nextcloud/

```

```bash
[user@web /]$ cat  /etc/httpd/conf/httpd.conf |tail -n 1
IncludeOptional conf.d/*.conf

[user@web /]$ sudo vim  /etc/httpd/conf.d/TP_5.conf
[user@web /]$ cat  /etc/httpd/conf.d/TP_5.conf
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp5_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp5.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp5_nextcloud/> 
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

```

```bash
[user@web /]$ sudo systemctl reload httpd && sudo systemctl status httpd | grep active
     Active: active (running) since Tue 2023-01-17 10:21:24 CET; 2h 14min ago

```

### 3. Finaliser l'installation de NextCloud


```bash
 toto@fedora37  ~  sudo vim /etc/hosts
 toto@fedora37  ~  cat /etc/hosts | tail -n 1
10.105.1.11 web.tp5.linux

```

```bash
 toto@fedora37  ~  curl web.tp5.linux | tail -n 4
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  6421    0  6421    0     0   272k      0 --:--:-- --:--:-- --:--:--  285k
				<a href="https://nextcloud.com" target="_blank" rel="noreferrer noopener">Nextcloud</a> – a safe home for all your data			</p>
		</footer>
	</body>
</html>

```

```bash
[user@web /]$  mysql -u nextcloud -h 10.105.1.12 -p

mysql> USE nextcloud
Database changed
mysql> SELECT Count(*)
    -> FROM INFORMATION_SCHEMA.TABLES
    -> WHERE TABLE_TYPE = 'BASE TABLE';
+----------+
| Count(*) |
+----------+
|       95 |
+----------+
1 row in set (0.01 sec)

```

### Partie 4 : Automatiser la résolution du TP

