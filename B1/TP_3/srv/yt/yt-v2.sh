#!/bin/bash
#01/09/2023
#Youtube-dl service-script by toto



logfile='/var/log/yt/download.log'
dl_dir='/srv/yt/downloads'
url_file='/srv/yt/url_file.txt'

if [[ ! -d "${dl_dir}" ]]
then
  echo "Directory /srv/yt/downloads not found"
  exit 0
fi

if [[ ! -f "${logfile}" ]]
then
  echo "File /var/log/yt/download.log do not exist"
  exit 0
fi

if [[ ! -f "${url_file}" ]]
then
  echo "File /srv/yt/url_file.txt do not exist"
  exit 0
fi

while true
do
  index_line="$(wc -l < /srv/yt/url_file.txt)"
  if [[ "${index_line}" -ne "0" ]]
  then
    url=$(cat /srv/yt/url_file.txt | head -n 1 | grep 'https://www.youtube.com/watch?v=')
    sed -i '1d' /srv/yt/url_file.txt
    title="$(/usr/local/bin/youtube-dl --get-title ${url})"
    extension="$(/usr/local/bin/youtube-dl --get-filename ${url} --restrict-filenames | cut -d'.' -f2)"
    mkdir /srv/yt/downloads/"${title}"
    /usr/local/bin/youtube-dl --get-description "${url}" > /srv/yt/downloads/"${title}"/description > /dev/null
    /usr/local/bin/youtube-dl -o /srv/yt/downloads/"${title}"/"${title}.${extension}" "${url}" > /dev/null
    echo [$(date "+%D %T")] Video "${url}" was downloaded. File path : /srv/yt/downloads/"${title}"/"${title}.${extension}" >> "${logfile}"
    echo "Video ${url} was downloaded."
    echo File path : /srv/yt/downloads/"${title}"/"${title}.${extension}"
  fi
  sleep 2
done
