#!/bin/bash
#01/09/2023
#Youtube-dl script by toto



logfile='/var/log/yt/download.log'
dl_dir='/srv/yt/downloads'

if [[ ! -d "${dl_dir}" ]]
then
  echo "Directory /srv/yt/downloads not found"
  exit 0
fi

if [[ ! -f "${logfile}" ]]
then
  echo "File /var/log/yt/download.log do not exist"
  exit 0
fi
video_url="$1"
title="$(youtube-dl --get-title ${video_url})"
extension="$(youtube-dl --get-filename ${video_url} --restrict-filenames | cut -d '.' -f2)"

mkdir /srv/yt/downloads/"${title}"
youtube-dl --get-description "${video_url}" > /srv/yt/downloads/"${title}"/description > /dev/null
youtube-dl -o /srv/yt/downloads/"${title}"/"${title}.${extension}" "${video_url}" > /dev/null

echo [$(date "+%D %T")] Video "${video_url}" was downloaded. File path : /srv/yt/downloads/"${title}"/"${title}.${extension}" >> "${logfile}"
echo "Video ${video_url} was downloaded."
echo "File path : /srv/yt/downloads/${title}/${title}.${extension}"
