#!/bin/bash
#01/03/2023
#Identity card script by toto


name="$(hostnamectl | grep 'Transient hostname' | cut -d ':' -f2)"
os="$(cat /etc/redhat-release)"
kernel="$(uname -r)"
ip="$(ip a | grep 'inet ' | tail -n 1 | cut -d ' ' -f6)"
ram_max="$(free | grep 'Mem:' | tr -s ' ' | cut -d ' ' -f2)"
ram_free="$(free | grep 'Mem:' | tr -s ' ' | cut -d ' ' -f4)"
disk_freespace="$(df -h | grep '/$' | tr -s ' ' | cut -d ' ' -f4)"

echo "Machine name :${name}"
echo "OS ${os} and kernel version is ${kernel}"
echo "IP : ${ip}"
echo "RAM : ${ram_free} memory available on ${ram_max} total memory"
echo "Disk : ${disk_freespace} space left"
echo "Top 5 processes by RAM usage :"

ps="$(ps -e -o cmd= --sort=-%mem | head -n5 | cut -d' ' -f1)"

	while read line
	do
	echo "  - ${line}"
	done <<< "${ps}"

echo "Listening ports :"
	while read line
	do
	port=$(echo "${line}" | awk '{print $5}' | cut -d ':' -f2)
	protocol=$(echo "${line}" | awk '{print $1}')
	service_running=$(echo "${line}" | awk '{print $7}' | cut -d '"' -f2)

		if [[ "Local" -ne ${port} ]]; then
		printf "  - %s %s : %s\n" "${port}" "${protocol}" "${service_running}"
		fi
	done <<< "$(ss -alnp4 | tr -s ' ')"

download_picture () {
	image_url="https://cataas.com/cat"
	destination_folder="./"
	sudo wget "${image_url}" -P "${destination_folder}" &> /dev/null
	image_file=$(basename "${image_url}")
	file_extension=$(file --extension "${image_file}" | cut -d ' ' -f2 | cut -d '/' -f1)
	printf "\nHere is your random cat : %s%s.%s\n" "${destination_folder}" "${image_file}" "${file_extension}"
	}

download_picture