# TP 3 : We do a little scripting

## 0. Un premier script

```bash
[user@localhost ~]$ sudo touch /srv/test.sh
[user@localhost ~]$ sudo vim /srv/test.sh
[user@localhost ~]$ cat /srv/test.sh
#!/bin/bash
# Simple test script

echo "Connecté actuellement avec l'utilisateur $(whoami)."

```

```bash
[user@localhost ~]$ ll /srv/test.sh
-rw-r--r--. 1 root root 95 Jan  3 10:41 /srv/test.sh
[user@localhost ~]$ sudo chown user /srv/test.sh 
[user@localhost ~]$ chmod 744 /srv/test.sh
[user@localhost ~]$ ll /srv/test.sh
-rwxr--r--. 1 user user 95 Jan  3 10:41 /srv/test.sh

```

```bash
[user@localhost ~]$ /srv/test.sh
Connecté actuellement avec l'utilisateur user.
[user@localhost ~]$ cd /srv
[user@localhost srv]$ ./test.sh
Connecté actuellement avec l'utilisateur user.

```

## I. Script carte d'identité

```bash
[user@localhost srv]$ sudo mkdir /srv/idcard
[user@localhost srv]$ sudo touch /srv/idcard/idcard.sh
[user@localhost srv]$ sudo vim /srv/idcard/idcard.sh

```

```bash
[user@localhost srv]$ cat /srv/idcard/idcard.sh

```
[Fichier de script](/srv/idcard/idcard.sh)

```bash
[user@localhost idcard]$ cd /srv/idcard/
[user@localhost idcard]$ sudo chown user idcard.sh 
[user@localhost idcard]$ chmod u+x idcard.sh 

```

```bash
[user@localhost idcard]$ sudo ./idcard.sh
Machine name : localhost
OS Rocky Linux release 9.1 (Blue Onyx) and kernel version is 5.14.0-162.6.1.el9_1.0.1.x86_64
IP : 10.3.1.7/24
RAM : 197980 memory available on 464248 total memory
Disk : 5.0G space left
Top 5 processes by RAM usage :
  - /usr/bin/python3
  - /usr/sbin/NetworkManager
  - /usr/lib/systemd/systemd
  - /usr/lib/systemd/systemd
  - /usr/lib/systemd/systemd-udevd
Listening ports :
  - 323 udp : 
  - 22 tcp : 

Here is your random cat : ./cat.png

```

## II. Script youtube-dl

```bash
[user@localhost idcard]$ cd
[user@localhost ~]$ sudo dnf install wget && sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl && sudo chmod a+rx /usr/local/bin/youtube-dl
[user@localhost ~]$ cd /srv
[user@localhost srv]$ sudo mkdir yt
[user@localhost srv]$ cd yt
[user@localhost yt]$ sudo touch yt.sh
[user@localhost yt]$ sudo chown user yt.sh
[user@localhost yt]$ chmod u+x yt.sh
[user@localhost yt]$ sudo mkdir /srv/yt/downloads
[user@localhost yt]$ sudo mkdir /var/log/yt
[user@localhost yt]$ sudo touch /var/log/yt/download.log
[user@localhost yt]$ sudo chown user /srv/yt/downloads
[user@localhost yt]$ sudo chown user /var/log/yt/download.log

```

```bash
[user@localhost yt]$ ./yt.sh 'https://www.youtube.com/watch?v=Wch3gJG2GJ4'
Video https://www.youtube.com/watch?v=Wch3gJG2GJ4 was downloaded.
File path : /srv/yt/downloads/1 Second Video/1 Second Video.webm

```

```bash
[user@localhost yt]$ cat yt.sh

```
[Fichier de script](/srv/yt/yt.sh)

```bash
[01/09/23 18:43:54] Video https://www.youtube.com/watch?v=Wch3gJG2GJ4 was downloaded. File path : /srv/yt/downloads/1 Second Video/1 Second Video.webm
[01/09/23 18:47:41] Video https://www.youtube.com/watch?v=jjs27jXL0Zs was downloaded. File path : /srv/yt/downloads/SI LA VIDÉO DURE 1 SECONDE LA VIDÉO S'ARRÊTE/SI LA VIDÉO DURE 1 SECONDE LA VIDÉO S'ARRÊTE.mp4

```

## III. MAKE IT A SERVICE

```bash
[user@localhost yt]$ sudo vim yt-v2.sh
[user@localhost yt]$ sudo vim url_file.txt
[user@localhost yt]$ sudo chown user yt-v2.sh
[user@localhost yt]$ sudo chown user url_file.txt 
[user@localhost yt]$ chmod u+x  yt-v2.sh 
[user@localhost yt]$ chmod +w  url_file.txt

```

```bash 
[user@localhost yt]$ cat yt-v2.sh 

```

[Fichier de script](/srv/yt/yt-v2.sh)

```bash
[user@localhost yt]$ cat url_file.txt 
https://www.youtube.com/watch?v=QT62DGw2OYo
https://www.youtube.com/watch?v=Wch3gJG2GJ4
https://www.youtube.com/watch?v=a8DM-tD9w2I
https://www.youtube.com/watch?v=tbnLqRW9Ef0
https://www.youtube.com/watch?v=jjs27jXL0Zs

```

```bash
[user@localhost ~]$ sudo vim /etc/systemd/system/yt.service

```
[Fichier du service](/etc/systemd/system/yt.service)

```bash
[user@localhost ~]$ sudo useradd yt
[user@localhost ~]$ cat /etc/passwd | grep yt
yt:x:1001:1001::/home/yt:/bin/bash
[user@localhost ~]$ sudo chown  yt /srv/yt/url_file.txt
[user@localhost ~]$ sudo chown  yt /srv/yt/yt-v2.sh 
[user@localhost ~]$ sudo chown yt /var/log/yt/
[user@localhost yt]$ sudo chown yt downloads

[user@localhost ~]$ sudo systemctl daemon-reload
[user@localhost ~]$ sudo systemctl enable yt
[user@localhost ~]$ sudo systemctl start yt
[user@localhost ~]$ sudo systemctl status yt
[user@localhost ~]$ sudo systemctl status  yt | grep active
     Active: active (running) since Tue 2023-01-10 22:51:35 CET; 43s ago
[user@localhost ~]$ journalctl -xe -u yt | tail -n 8
Jan 10 22:51:35 localhost.localdomain systemd[1]: Started <Read a link file to permanently download youtube videos>.
░░ Subject: A start job for unit yt.service has finished successfully
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░ 
░░ A start job for unit yt.service has finished successfully.
░░ 
░░ The job identifier is 1640.

```

[![asciicast](https://asciinema.org/a/551000.svg)](https://asciinema.org/a/551000)