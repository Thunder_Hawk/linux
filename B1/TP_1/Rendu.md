# II. Feu

## Using nano

```
sudo nano /etc/profile

while :         
do
echo Goodbye !
logout
done

```
Deconnexion en boucle. Simple, marrant et personnalisable (en effet, on peut être bien moins cordiale en changeant le "Au-revoir !")

## Using vim

```
sudo vim /boot/vmlinuz-5.14.0-70.26.1.el9_0.x86_64
```
Et on vide, ou on remplace le contenu...

```
sudo reboot
```

Erreur sur le lancement de CETTE VERSION sur le grub ; GG WP ! (A noter que dans le dossier "boot" on retrouve les autres versions bootables au démarrage du grub, il est plus marrant de modifier les trois ; bien évidemment)
Zut, moi qui pensais que c'était un fichier où on pouvoir s'exprimer librement sans soucis...

## Using systemctl

```
for service in $(systemctl -t service | grep service | awk ("print $1") | grep service);
do
sudo systemctl disable $service;
sudo systemctl stop $service;
done 
```

Petite boucle pas piquée des hannetons qui désactive et stop TOUS les services UN à UN ; donc oui, ça marche moins bien ! (lol)

## Using rm :)

```
sudo rm /etc/shadow
```
Login incorrect ;) Try again I guess . . .
(Facile à réparer, mais suffisant pour faire ressortir le "carnet de mots de passes" de Brigitte, 54 ans.)

## Using dd

```
sudo dd if=/dev/zero of=/dev/sda5
```

"Petit" coup de balais dans les données du disque... (Ça fait beaucoup (de zéros) là, non ?)