## I. Service SSH

### 1. Analyse du service

```bash
[user@TP2 ~]$ systemctl status sshd | grep "active"
     Active: active (running) since Fri 2022-12-09 15:44:04 CET; 4min 10s ago

```

```bash
[user@TP2 ~]$ ps -ef | grep "sshd"
root         689       1  0 15:44 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root         853     689  0 15:44 ?        00:00:00 sshd: user [priv]
user         857     853  0 15:44 ?        00:00:00 sshd: user@pts/0
user         884     858  0 15:50 pts/0    00:00:00 grep --color=auto ssh

```

```bash
[user@TP2 ~]$ ss | grep "ssh"
tcp   ESTAB  0      0                         10.3.1.5:ssh           10.3.1.1:45728 
```
```bash
[user@TP2 ssh]$ journalctl -xe -u sshd
Dec 09 15:44:04 TP2 systemd[1]: Starting OpenSSH server daemon...
░░ Subject: A start job for unit sshd.service has begun execution
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░ 
░░ A start job for unit sshd.service has begun execution.
░░ 
░░ The job identifier is 230.
Dec 09 15:44:04 TP2 sshd[689]: Server listening on 0.0.0.0 port 22.
Dec 09 15:44:04 TP2 sshd[689]: Server listening on :: port 22.
Dec 09 15:44:04 TP2 systemd[1]: Started OpenSSH server daemon.
░░ Subject: A start job for unit sshd.service has finished successfully
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░ 
░░ A start job for unit sshd.service has finished successfully.
░░ 
░░ The job identifier is 230.
Dec 09 15:44:26 TP2 sshd[853]: Accepted password for user from 10.3.1.1 port 45728 ssh2
Dec 09 15:44:26 TP2 sshd[853]: pam_unix(sshd:session): session opened for user user(uid=1000) by (uid=0)

```

```bash
[user@TP2 ssh]$ sudo tail  /var/log/secure | grep ssh
Dec  9 16:58:53 TP2 sudo[1171]:    user : TTY=pts/0 ; PWD=/etc/ssh ; USER=root ; COMMAND=/bin/systemctl restart sshd
Dec  9 16:58:53 TP2 sshd[1168]: Received signal 15; terminating.
Dec  9 16:58:53 TP2 sshd[1175]: Server listening on 0.0.0.0 port 29500.
Dec  9 16:58:53 TP2 sshd[1175]: Server listening on :: port 29500.
Dec  9 17:00:18 TP2 sudo[1176]:    user : TTY=pts/0 ; PWD=/etc/ssh ; USER=root ; COMMAND=/bin/cat /var/log/secure

```

> Refait en cours de TP, RIP you have been spoiled !

### 2. Modification du service

```bash
[user@TP2 ~]$ echo $RANDOM
29500

[user@TP2 ~]$ sudo vim /etc/ssh/sshd_config

```
> Trouvé

```bash
[user@TP2 log]$ echo $RANDOM
29500

```

```bash
[user@TP2 log]$ echo $RANDOM
29500
```
```bash
[user@TP2 ssh]$ sudo cat /etc/ssh/sshd_config | grep Port
#Port 29500
[user@TP2 ssh]$ sudo vim /etc/ssh/sshd_config.d/*.conf
[user@TP2 ssh]$ sudo cat  /etc/ssh/sshd_config.d/*.conf
Port 29500
```

```bash
[user@TP2 ssh]$ sudo firewall-cmd --remove-service ssh --permanent
success

[user@TP2 ssh]$ sudo firewall-cmd --add-port=29500/tcp --permanent
success
[user@TP2 ssh]$ sudo firewall-cmd --reload
success
[user@TP2 ssh]$ sudo firewall-cmd --list-all | grep 29500
  ports: 29500/tcp

```

```bash
[user@TP2 ssh]$ sudo systemctl restart sshd
```
```bash
 ✘ toto@fedora37  ~  ssh user@10.3.1.5 -p 29500
user@10.3.1.5's password:

```

> D'après google, il y a plusieurs façons de sécuriser ssh ; à fin de proteger ma VM (chérie) je vais limiter la connection ssh à mon IP, sur mon port 25900 au lieu du 22.


```bash
[user@TP2 ~]$ sudo firewall-cmd --zone=internal --remove-service=ssh --permanent
[user@TP2 ~]$ sudo firewall-cmd --zone=internal --add-source=10.3.1.1/32
[user@TP2 ~]$ sudo firewall-cmd --zone=public --remove-service=ssh
[user@TP2 ~]$ sudo firewall-cmd --zone=public --remove-service=ssh --permanent
[user@TP2 ~]$ sudo firewall-cmd --reload


```

## II. Service HTTP

### 1. Mise en place

```bash
[user@TP2 ~]$ sudo dnf install nginx -y

[user@TP2 ~]$ sudo systemctl enable nginx
[user@TP2 ~]$ sudo systemctl start nginx

```

```bash
[user@TP2 ~]$ sudo firewall-cmd --zone=internal --permanent --add-port=80/tcp
succes
[user@TP2 ~]$ sudo firewall-cmd --reload
success
[user@TP2 ~]$ systemctl status nginx | grep active
     Active: active (running) since Sat 2022-12-10 14:12:41 CET; 7min ago

```

```bash
[user@TP2 ~]$ ps -ef | grep "nginx"
root       10773       1  0 14:12 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      10774   10773  0 14:12 ?        00:00:00 nginx: worker process
user       10885     908  0 14:30 pts/0    00:00:00 grep --color=auto nginx
[user@TP2 ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success
[user@TP2 ~]$ sudo firewall-cmd --reload
success

```

```bash
[user@TP2 ~]$ sudo vim /etc/nginx/nginx.conf
[user@TP2 ~]$ sudo cat  /etc/nginx/nginx.conf | grep listen
        listen       80;
        listen       [::]:80;
#        listen       80 ssl http2;
#        listen       [::]:80 ssl http2;
[user@TP2 ~]$ sudo systemctl reload nginx

```
> Par défaut c'était le port 80 ; que finalement j'ai laissé *clown*

```bash
[user@TP2 ~]$ ss -alnpt
State           Recv-Q          Send-Q                    Local Address:Port                      Peer Address:Port          Process          
LISTEN          0               511                             0.0.0.0:80                          0.0.0.0:*                              
LISTEN          0               128                            10.3.1.5:29500                          0.0.0.0:*                              
LISTEN          0               511                                [::]:80                             [::]:*      
```

```bash 
 toto@fedora37  ~  curl 10.3.1.5 | grep head
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0  10  <head>
.5M      0  </head>
 --:--:-- --:--:-- --:--:-- 7441k

```

### 2. Analyser la conf de NGINX

```bash
[user@TP2 ~]$ ls -al /etc/nginx/nginx.conf
-rw-r--r--. 1 root root 2334 Oct 31 16:37 /etc/nginx/nginx.conf

```
```bash
[user@TP2 ~]$ cat  /etc/nginx/nginx.conf | grep "server {" -A 18
    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }

# Settings for a TLS enabled server.
--
#    server {
#        listen       443 ssl http2;
#        listen       [::]:443 ssl http2;
#        server_name  _;
#        root         /usr/share/nginx/html;
#
#        ssl_certificate "/etc/pki/nginx/server.crt";
#        ssl_certificate_key "/etc/pki/nginx/private/server.key";
#        ssl_session_cache shared:SSL:1m;
#        ssl_session_timeout  10m;
#        ssl_ciphers PROFILE=SYSTEM;
#        ssl_prefer_server_ciphers on;
#
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
#        error_page 404 /404.html;
#            location = /40x.html {
#        }


```

```bash
[user@TP2 ~]$ cat  /etc/nginx/nginx.conf | grep include
include /usr/share/nginx/modules/*.conf;
    include             /etc/nginx/mime.types;
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/default.d/*.conf;
#        include /etc/nginx/default.d/*.conf;

```

### 3. Déployer un nouveau site web

```bash
[user@TP2 ~]$ cd /var/
[user@TP2 var]$ sudo mkdir www
[user@TP2 var]$ cd www
[user@TP2 www]$ sudo mkdir tp2_linux
[user@TP2 www]$ cd tp2_linux/
[user@TP2 tp2_linux]$ sudo touch index.html
[user@TP2 tp2_linux]$ sudo vim index.html
[user@TP2 www]$ cat index.html 
<h1>MEOW mon [second] serveur web</h1>

```

```ssh
 toto@fedora37  ~  echo $RANDOM
25612
```

```bash
[user@TP2 ~]$ sudo vim /etc/nginx/nginx.conf
[user@TP2 ~]$ cat /etc/nginx/nginx.conf | grep 'server {'
#    server {

```

```bash
[user@TP2 ~]$ sudo touch /etc/nginx/conf.d/meow_index.conf
[user@TP2 ~]$ sudo vim /etc/nginx/conf.d/meow_index.conf
[user@TP2 ~]$ cat /etc/nginx/conf.d/meow_index.conf
server {
  listen 25612;

  root /var/www/tp2_linux;
}

```

```bash
[user@TP2 ~]$ sudo systemctl reload nginx
[user@TP2 ~]$ curl 10.3.1.5:25612
<h1>MEOW mon [second] serveur web</h1>
[user@TP2 ~]$ sudo firewall-cmd --zone=internal --permanent --add-port=25612/tcp
success
[user@TP2 ~]$ sudo firewall-cmd --reload
success

```

```bash
 toto@fedora37  ~  curl 10.3.1.5:25612
<h1>MEOW mon [second] serveur web</h1>

```

>Meow ?! 

## III. Your own services

### 1. Au cas où vous auriez oublié

```bash
[user@TP2 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent --zone=internal
success
[user@TP2 ~]$ sudo firewall-cmd --add-port=8888/tcp --zone=internal
success
```

```bash
 toto@fedora37  ~  nc 10.3.1.5 8888
lol
le sanglier
t'écoute vraiment n'importe quoi !
```
> Oui c'est pas une VM, mais Léo il a dit que je pouvais :p

```bash
[user@TP2 ~]$ nc -l 8888
lol
le sanglier
t'écoute vraiment n'importe quoi !
```
> Pour les plus courageux : Le sanglier - DaPoule
### 2. Analyse des services existants

```bash
[user@TP2 ~]$ systemctl status sshd | grep Loaded:
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
[user@TP2 ~]$ cat /usr/lib/systemd/system/sshd.service | grep ExecStart=
ExecStart=/usr/sbin/sshd -D $OPTIONS
[user@TP2 ~]$ systemctl status nginx | grep Loaded:
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
[user@TP2 ~]$ cat /usr/lib/systemd/system/nginx.service | grep ExecStart=
ExecStart=/usr/sbin/nginx

```

### 3. Création de service

```bash
 ✘ toto@fedora37  ~  echo $RANDOM       
26598
```

```bash
[user@TP2 ~]$ sudo firewall-cmd --add-port=26598/tcp --permanent --zone=internal
success
[user@TP2 ~]$ sudo firewall-cmd --add-port=26598/tcp --zone=internal
success

```

```bash
[user@TP2 ~]$ sudo touch /etc/systemd/system/tp2_nc.service
[user@TP2 ~]$ sudo vim /etc/systemd/system/tp2_nc.service
[user@TP2 ~]$ cat  /etc/systemd/system/tp2_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 26598

[user@TP2 ~]$ sudo systemctl daemon-reload
[user@TP2 ~]$ sudo systemctl start tp2_nc
[user@TP2 ~]$ systemctl status tp2_nc | grep active
     Active: active (running) since Tue 2022-12-13 21:32:50 CET; 29s ago

```

```bash
[user@TP2 ~]$ sudo ss -alnpt |grep nc
LISTEN 0      10           0.0.0.0:26598      0.0.0.0:*    users:(("nc",pid=1748,fd=4))                           
LISTEN 0      10              [::]:26598         [::]:*    users:(("nc",pid=1748,fd=3)) 

```

``` bash
[user@TP2 ~]$ sudo journalctl -xe -u tp2_nc |grep 'Started'
Dec 13 21:19:53 TP2 systemd[1]: Started Super netcat tout fou.
[user@TP2 ~]$ sudo journalctl -xe -u tp2_nc |grep 1748
Dec 13 21:39:57 TP2 nc[1748]: hello
Dec 13 21:40:27 TP2 nc[1748]: bon, pourquoi on est sur du sardou maintenant ?!
[user@TP2 ~]$ sudo journalctl -xe -u tp2_nc | grep 'tp2_nc.service: Deactivated successfully.'
Dec 13 21:40:29 TP2 systemd[1]: tp2_nc.service: Deactivated successfully.

```

```bash
[user@TP2 ~]$ sudo vim /etc/systemd/system/tp2_nc.service
[user@TP2 ~]$ cat /etc/systemd/system/tp2_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 26598
Restart=always
[user@TP2 ~]$ sudo systemctl daemon-reload
[user@TP2 ~]$ 

```