# TP6 : Travail autour de la solution NextCloud

## Module 1 : Reverse Proxy

```bash
[user@proxy ~]$ sudo dnf install nginx -y && sudo systemctl start nginx && sudo ss -altnp4 |grep nginx&& ps -ef | grep nginx && curl 127.0.0.1 | tail -n 5

Complete!

LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1628,fd=6),("nginx",pid=1626,fd=6))

root        1626       1  0 10:15 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1628    1626  0 10:15 ?        00:00:00 nginx: worker process
user       10804     858  0 10:21 pts/0    00:00:00 grep --color=auto nginx

user       10795     858  0 10:17 pts/0    00:00:00 grep --color=auto NGINX
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0   676k      0 --:--:-- --:--:-- --:--:--  676k
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>
      
  </body>
</html>

```

```bash
[user@proxy ~]$ cd /etc/nginx/
[user@proxy nginx]$ cd conf.d/
[user@proxy nginx]$ cat nginx.conf | grep include | head -n 4 | tail -n 1
    include /etc/nginx/conf.d/*.conf;

[user@proxy nginx]$ cd
```

```bash
[user@proxy ~]$ cat /etc/nginx/conf.d/xnign.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp5.linux;

    # Port d'écoute de NGINX
    listen 80;


    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying 
        proxy_pass http://10.105.1.11:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}

```

```bash
[user@proxy ~]$ sudo systemctl restart nginx
[user@proxy ~]$ sudo systemctl status  nginx | grep active

```

```bash
[user@proxy ~]$ sudo firewall-cmd --add-port 80/tcp --permanent && sudo firewall-cmd --reload
success
success
```

```bash
[user@web ~]$ sudo  cat /var/www/tp5_nextcloud/config/config.php | tail -n 17 |head -n 4
  array (
	  0 => 'web.tp5.linux',
	  1 => 'www.nextcloud.tp5',
  ),

```

```bash


```bash
[user@web ~]$ sudo systemctl restart httpd
[user@web ~]$ sudo systemctl status httpd | grep active
     Active: active (running) since Mon 2023-02-06 19:51:52 CET; 24s ago

```

```bash
[user@web ~]$ sudo firewall-cmd --set-default-zone drop
success
[user@web ~]$ sudo firewall-cmd --get-default-zone
drop
[user@web ~]$ sudo firewall-cmd --add-source=10.105.1.13 --permanent
success
[user@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[user@web ~]$ sudo firewall-cmd --reload
success

```

```bash
[user@web ~]$ sudo firewall-cmd --list-all
[sudo] password for user: 
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 10.105.1.13 10.105.1.1
  services: 
  ports: 80/tcp 22/tcp
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
  
```

```zsh
 ✘ toto@fedora37  ~  ping 10.105.1.11
PING 10.105.1.11 (10.105.1.11) 56(84) octets de données.
^C

 ✘ toto@fedora37  ~  ping 10.105.1.13
PING 10.105.1.13 (10.105.1.13) 56(84) octets de données.
64 octets de 10.105.1.13 : icmp_seq=1 ttl=64 temps=0.581 ms
64 octets de 10.105.1.13 : icmp_seq=2 ttl=64 temps=0.668 ms
^C

```

```bash
[user@proxy ~]$ cd /etc/nginx/
[user@proxy nginx]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout www.nextcloud.tp5.key -out www.nextcloud.tp5.crt

```

```bash
[user@proxy ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent &&
sudo firewall-cmd --add-port=443/tcp --permanent &&  
sudo firewall-cmd --reload 
success
success
success

```

```bash
[user@proxy ~]$ sudo vim /etc/nginx/conf.d/xnign.conf 
[user@proxy ~]$ cat /etc/nginx/conf.d/xnign.conf  | head -n 11 | tail -n 8
    server_name www.nextcloud.tp5;

    # Port d'écoute de NGINX
    listen 443 ssl;
        ssl_certificate     www.nextcloud.tp5.crt;
    ssl_certificate_key www.nextcloud.tp5.key;
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers         HIGH:!aNULL:!MD5;

```

```bash
[user@proxy ~]$ sudo systemctl restart nginx
[user@proxy ~]$ sudo systemctl status nginx | grep active
     Active: active (running) since Mon 2023-02-06 20:51:09 CET; 7s ago

```

```bash
[user@web ~]$ cat /etc/httpd/conf.d/TP_5.conf | head -n 1
<VirtualHost *:443>

```
```zsh
 toto@fedora37  ~  curl https://www.nextcloud.tp5 -k -L |tail -n 5
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0   643k      0 --:--:-- --:--:-- --:--:--  676k
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>
      
  </body>
</html>

```

# Module 2 : Sauvegarde du système de fichiers


```bash
[user@web ~]$ sudo vim /srv/tp5_backup.sh
[user@web ~]$ cat /srv/tp5_backup.sh
#!/bin/bash
# 12/02/2023
# Nextcloud's backup script by toto

/usr/bin/php81 /var/www/tp5_nextcloud/occ maintenance:mode --on

backup_folder="/srv/backup/"
backup_ext=".tar.gz"
backup_release=$(date +"%Y%m%d")

nc_conf="/var/www/tp5_nextcloud/config/"
nc_data="/var/www/tp5_nextcloud/data/"
nc_themes="/var/www/tp5_nextcloud/themes/"

tar -czvf "${backup_folder}/nextcloud_${backup_release}${backup_ext}" "${nc_conf}" "${nc_data}" "${nc_themes}"

/usr/bin/php81 /var/www/tp5_nextcloud/occ maintenance:mode --off

```

```bash
[user@web ~]$ sudo useradd backup -m -d /srv/backup -s /usr/sbin/nologin
[user@web ~]$ sudo passwd backup 
[user@web ~]$ sudo usermod -aG wheel backup
[user@web ~]$ sudo chown backup -R /srv/backup/
[user@web ~]$ sudo chown backup -R /srv/tp5_backup.sh

[user@web ~]$ ls -al /srv
total 4
drwxr-xr-x.  3 root   root    41 Feb 13 09:41 .
dr-xr-xr-x. 18 root   root   235 Oct 26 08:09 ..
drwx------.  2 backup backup  62 Feb 13 09:41 backup
-rw-r--r--.  1 backup root   512 Feb 13 09:38 tp5_backup.sh

```

```bash
[user@web ~]$ sudo dnf install tar
[user@web ~]$ sudo chown -R backup:apache /var/www/tp5_nextcloud && sudo chmod -R 770 /var/www/tp5_nextcloud

[user@web ~]$ sudo -u backup bash /srv/tp5_backup.sh
[user@web ~]$ sudo -u backup bash /srv/tp5_backup.sh
The current PHP memory limit is below the recommended value of 512MB. 
                                                                   
maintenance:mode [--on] [--off]

tar: Removing leading `/' from member names
/var/www/tp5_nextcloud/config/
/var/www/tp5_nextcloud/config/config.sample.php

[...]

/var/www/tp5_nextcloud/themes/README
The current PHP memory limit is below the recommended value of 512MB.

```

```bash
[user@web ~]$ sudo vim /etc/systemd/system/backup.service
[user@web ~]$ cat /etc/systemd/system/backup.service
[Unit]
Description=Nextcloud's backup service
 
[Service]
Type=oneshot
User=backup 
ExecStart=bash /srv/tp5_backup.sh

[Install]
WantedBy=multi-user.target

```

```bash
[user@web ~]$ sudo systemctl daemon-reload
[user@web ~]$ sudo systemctl start backup
[user@web ~]$ sudo systemctl status backup | grep active
     Active: inactive (dead)

```

```bash
[user@web ~]$ sudo vim /etc/systemd/system/backup.timer
[user@web ~]$ cat /etc/systemd/system/backup.timer
[Unit]
Description=Run service backup

[Timer]
OnCalendar=*-*-* 6:00:00

[Install]
WantedBy=timers.target

[user@web ~]$ sudo systemctl daemon-reload
[user@web ~]$ sudo systemctl start backup.timer
[user@web ~]$ sudo systemctl enable backup.timer
Created symlink /etc/systemd/system/timers.target.wants/backup.timer → /etc/systemd/system/backup.timer.
[user@web ~]$ sudo systemctl status backup.timer | grep active
     Active: active (waiting) since Wed 2023-02-15 20:24:26 CET; 36s ago

```


```bash
[user@storage ~]$ sudo mkdir /srv/nfs_shares/web.tp5.linux/ -p
[user@db ~]$ sudo dnf install nfs-utils -y

[user@storage ~]$ sudo chown -R nobody /srv/nfs_shares/
[user@storage ~]$ sudo vim /etc/exports
[user@storage ~]$ cat  /etc/exports
/srv/nfs_shares/web.tp5.linux/    10.105.1.11(rw,sync,no_subtree_check)

[user@storage ~]$ sudo firewall-cmd --permanent --add-service=nfs
success
[user@storage ~]$ sudo firewall-cmd --permanent --add-service=mountd
success
[user@storage ~]$ sudo firewall-cmd --permanent --add-service=rpc-bind
success
[user@storage ~]$ sudo firewall-cmd --reload
success

```

```bash
[user@storage ~]$ sudo systemctl start nfs-server
[user@storage ~]$ sudo systemctl enable nfs-server
[user@storage ~]$ systemctl status nfs-server | grep active
     Active: active (exited) since Wed 2023-02-15 20:51:21 CET; 3min 30s ago

```

```bash
[user@web ~]$ sudo dnf install nfs-utils -y
[user@web ~]$ sudo mount 10.105.1.14:/srv/nfs_shares/web.tp5.linux/ /srv/backup
[user@web ~]$ cat /etc/fstab | tail -n 1

10.105.1.14:/srv/nfs_shares/web.tp6.linux/ /srv/backup nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0

[user@web ~]$ sudo tar -xvf /srv/backup/backup.tar.gz -C /srv/backup/localbak
[user@web ~]$ sudo rsync -Aax /srv/backup/localbak/config /nextcloud/config
[user@web ~]$ sudo rsync -Aax /srv/backup/localbak/data /nextcloud/data
[user@web ~]$ sudo rsync -Aax /srv/backup/localbak/themes /nextcloud/themes

```

# Module 3 : Fail2Ban


```bash
[user@db ~]$ sudo dnf install epel-release -y && sudo dnf install fail2ban -y
[user@db ~]$ sudo vim  /etc/fail2ban/jail.local
[user@db ~]$ cat /etc/fail2ban/jail.local | head -n 3
findtime= 1m
maxretry=3

```
 
 ```bash
[user@web ~]$ ssh user@10.105.1.12
ssh: connect to host 10.105.1.12 port 22: Connection refused

 ```

 ```bash
 [user@db ~]$ sudo fail2ban-client banned
[{'sshd': ['10.105.1.11']}]

[user@db ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     7
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 1
   |- Total banned:     2
   `- Banned IP list:   10.105.1.11


   [user@db ~]$ sudo fail2ban-client set sshd unbanip 10.105.1.11
1

 ```

 # Module 4 : Monitoring

 ```bash
[user@web ~]$ sudo systemctl status netdata | grep active
     Active: active (running) since Mon 2023-02-15 22:18:27 CET; 17min ago

 ```

 ```bash
[user@db ~]$ sudo systemctl status netdata | grep active
     Active: active (running) since Mon 2023-02-15 22:20:40 CET; 16min ago
 ```

 ```bash
[user@web ~]$ sudo ss -alnpt | grep netdata
LISTEN 0      4096         0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=26476,fd=6)) 
 ```

 ```bash
[user@web ~]$ ps -aux | grep netdata.p
netdata    13303  2.5  4.7 354992 46332 ?        Ssl  22:32   0:00 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
netdata    13448  0.0  0.3   4504  3424 ?        S    22:32   0:00 /usr/bin/bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1
netdata    13466  1.4  0.5 133452  5580 ?        Sl   22:32   0:00 /usr/libexec/netdata/plugins.d/apps.plugin 1
netdata    13469  5.6  2.6  30468 25952 ?        S    22:32   0:00 /usr/bin/python3 /usr/libexec/netdata/plugins.d/python.d.plugin 1
user       13637  0.0  0.1   6272  1072 pts/0    R+   22:32   0:00 grep --color=auto netdata.p

 ```


 ```bash
 [user@web ~]$ sudo cat /etc/netdata/netdata.conf | grep ' port '
	# default port = 19999
	# default port = 8125

 ```

 ```bash
[user@web ~]$ sudo cat /etc/netdata/health_alarm_notify.conf | head -6
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

 ```