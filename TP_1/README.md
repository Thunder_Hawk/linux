# TP1 : Premiers pas Docker

## Init


### 3. sudo c pa bo

#### 🌞 Ajouter votre utilisateur au groupe docker

```
$ sudo usermod  -aG docker toto
$ newgrp docker
```

### 4. Un premier conteneur en vif

#### 🌞 Lancer un conteneur NGINX

```bash
$ docker run -d -p 9999:80 nginx
```

```bash
$ docker ps

CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                   NAMES
578b94f49760   nginx     "/docker-entrypoint.…"   26 seconds ago   Up 26 seconds   0.0.0.0:9999->80/tcp, :::9999->80/tcp   wizardly_cartwright
```

```bash
$ docker logs 578 | tail

[...]
/docker-entrypoint.sh: Configuration complete; ready for start up

```

```bash
$ docker inspect 578 | grep MacA

            "MacAddress": "02:42:ac:11:00:02",
                    "MacAddress": "02:42:ac:11:00:02",

```

```bash
$ sudo ss -lnpt | grep 9999

LISTEN 0      4096              0.0.0.0:9999       0.0.0.0:*    users:(("docker-proxy",pid=6279,fd=4))    
LISTEN 0      4096                 [::]:9999          [::]:*    users:(("docker-proxy",pid=6285,fd=4))    
```

```bash
$ curl http://127.0.0.1:9999/ | grep Welcome

<title>Welcome to nginx!</title>
<h1>Welcome to nginx!</h1>
```

#### 🌞 On va ajouter un site Web au conteneur NGINX

> Je passe la creation de fichiers (copié collé des exemples du tp)

```bash
$ docker run -d -p 9999:8080 -v /home/toto/nginx/index.html:/var/www/html/index.html -v /home/toto/nginx/site_nul.conf:/etc/nginx/conf.d/site_nul.conf nginx
```

#### 🌞 Visitons

```bash
$ curl http://127.0.0.1:9999/ | grep MEO

<h1>MEOOOW</h1>
```

### 5. Un deuxième conteneur en vif
#### 🌞 Lance un conteneur Python, avec un shell

```bash
$ docker run -it python bash
```


#### 🌞 Installe des libs Python

```bash
root@14cd29ce005a:/# pip install aiohttp
```

```bash
root@14cd29ce005a:/# python
Python 3.12.1 (main, Dec 19 2023, 20:14:15) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import aiohttp
```

## II. Images

### 1. Images publiques

#### 🌞 Récupérez des images

```bash
$ docker pull python:3.11

$ docker pull mysql:5.7

$ docker pull wordpress:latest

$ docker pull linuxserver/wikijs:latest

$ docker images
REPOSITORY           TAG       IMAGE ID       CREATED       SIZE
linuxserver/wikijs   latest    218ef3649e4d   2 days ago    441MB
mysql                5.7       5107333e08a8   3 weeks ago   501MB
wordpress            latest    9071407ed1c0   4 weeks ago   740MB
python               3.11      22140cbb3b0c   4 weeks ago   1.01GB

```

#### 🌞 Lancez un conteneur à partir de l'image Python

```bash
$ docker run -it python:3.11 bash
root@d2510d221329:/# python --version
Python 3.11.7

```

### 2. Construire une image

#### 🌞 Ecrire un Dockerfile pour une image qui héberge une application Python


```dockerfile
FROM debian

RUN apt update -y
RUN apt install python3 -y
RUN apt install python3-pip -y

RUN pip install emoji --break-system-packages

WORKDIR /app
COPY app.py /app/app.py

ENTRYPOINT ["python3", "app.py"]
```

### 🌞 Build l'image

```bash 
$ cd python_app_build
$ docker build . -t python_app:version_de_ouf

$ docker images
REPOSITORY           TAG              IMAGE ID       CREATED         SIZE
python_app           version_de_ouf   68a9b9b5f1bb   2 minutes ago   638MB

```
### 🌞 Lancer l'image

```bash
$ docker run python_app:version_de_ouf
Cet exemple d'application est vraiment naze 👎

```

## III. Docker compose

### 🌞 Créez un fichier docker-compose.yml


```dockerfile
version: "3"

services:
  conteneur_nul:
    image: debian
    entrypoint: sleep 9999
  conteneur_flopesque:
    image: debian
    entrypoint: sleep 9999

```

### 🌞 Lancez les deux conteneurs avec docker compose

```bash
$ pwd
/home/toto/compose_test
$ docker compose up -d

```

### 🌞 Vérifier que les deux conteneurs tournent

```bash
$ docker compose ps
NAME                                 IMAGE     COMMAND        SERVICE               CREATED          STATUS          PORTS
compose_test-conteneur_flopesque-1   debian    "sleep 9999"   conteneur_flopesque   49 seconds ago   Up 47 seconds   
compose_test-conteneur_nul-1         debian    "sleep 9999"   conteneur_nul         49 seconds ago   Up 48 seconds

```
### 🌞 Pop un shell dans le conteneur conteneur_nul

```bash
$ docker exec -it compose_test-conteneur_nul-1 bash
root@619fa3c07d81:/#
root@619fa3c07d81:/# apt update -y && apt install iputils-ping -y
root@619fa3c07d81:/# ping conteneur_flopesque
PING conteneur_flopesque (172.18.0.3) 56(84) bytes of data.
64 bytes from compose_test-conteneur_flopesque-1.compose_test_default (172.18.0.3): icmp_seq=1 ttl=64 time=0.108 ms

```
